---
title: Lightweight CRM Long Description
author: Thiago Cangussu
date: 2021-05-31
language: en-US
---

## In a nutshell

* This template is a perfect fit for freelancers, founders, entrepreneurs, and business owners who want a lightweight solution to manage their business relationships.

* This template lets you manage people you met and track the opportunities and activities you have to build a business relationship with them.

* This template is a perfect fit if you are looking for a lightweight CRM as a starting point that you can rapidly set up and customize to fit your workflow.

## Foreword

While CRM tools provide a solid relationship management solution, most CRM tools make too many assumptions about how a CRM should work for you, forcing you to work in a way that doesn't fit your workflow.

More than often, these tools can be overly complex, meaning you end up with a huge number of features you don't put in use because you don't have the time to learn all of them.

This lightweight CRM is perfect for freelancers, founders, entrepreneurs, and small business owners who need to manage relationships of all types.

This template will give you a starting point to manage many different important strategic relationships for your business and you can and customize it to fit your unique needs.

## The Lightweight CRM template

This template consists of 4 sheets which translates to 4 tables in your database. These sheets are:

* Contacts: let you record people you met or were referred to you
* Companies: let you record the organizations these people belong
* Pipelines: let you record the opportunities you have with these people and track the progress you've made
* Activities: let you track the activities you've done related to people you've met

Each table is designed to make as few assumptions as possible about the information you want to record so they only include fields that are necessary to help you start as soon as possible to manage your business relationships.

This design helps you start quickly and stay out of your way for you to customize the template in a way that fits best in your workflow.

### Contacts table

This table records information about people you meet at events, people referred to you by business partners, coworkers, or anyone who could potentially become your client or business partner.

You can record the name of your contacts and a few details like job title and company they belong to, email and phone number. You can customize this sheet to include more information about your contact if you do like so.

This sheet also shows you all activities you've had with a particular contact and are stored in the Activities sheet.

### Companies table

This table records information about the companies people belong to. It makes few assumptions about the information you want to track, so it records just the name of the companies and a few details like address, website and the industry these companies belong to.

This table is related to the Contacts table and the Pipelines table so it shows you all contacts that belong to a company and all opportunities you are working with them.

This table is also completely customizable. You can add any information you think is relevant to keep track.

### Pipelines table

This table records all opportunities you have to establish a business relationship and tracks the status and other relevant information about these opportunities so you can know where you are in your pipeline to plan your next move accordingly.

This table has a direct connection to the Companies table so every opportunity you have is tied to a company.

### Activities table

This table records all activities you've done related to the opportunities you are tracking on the Pipelines table. Each activity is related to one or more contacts stored in the Contacts table.
